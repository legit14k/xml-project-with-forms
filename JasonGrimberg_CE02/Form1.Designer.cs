﻿namespace JasonGrimberg_CE02
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripCustomEvents = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDisplay = new System.Windows.Forms.ToolStripMenuItem();
            this.gbNewStudent = new System.Windows.Forms.GroupBox();
            this.rbFemale = new System.Windows.Forms.RadioButton();
            this.rbMale = new System.Windows.Forms.RadioButton();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblStudent = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.numAge = new System.Windows.Forms.NumericUpDown();
            this.cmboStudent = new System.Windows.Forms.ComboBox();
            this.rbEnglish = new System.Windows.Forms.RadioButton();
            this.rbMath = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.gbMath = new System.Windows.Forms.GroupBox();
            this.listMath = new System.Windows.Forms.ListView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.gbEnglish = new System.Windows.Forms.GroupBox();
            this.listEnglish = new System.Windows.Forms.ListView();
            this.gbClassSelection = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnToEnglish = new System.Windows.Forms.Button();
            this.btnToMath = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.menuStripCustomEvents.SuspendLayout();
            this.gbNewStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).BeginInit();
            this.gbMath.SuspendLayout();
            this.gbEnglish.SuspendLayout();
            this.gbClassSelection.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripCustomEvents
            // 
            this.menuStripCustomEvents.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStripCustomEvents.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.listToolStripMenuItem});
            this.menuStripCustomEvents.Location = new System.Drawing.Point(0, 0);
            this.menuStripCustomEvents.Name = "menuStripCustomEvents";
            this.menuStripCustomEvents.Size = new System.Drawing.Size(559, 24);
            this.menuStripCustomEvents.TabIndex = 3;
            this.menuStripCustomEvents.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLoad,
            this.menuSave,
            this.menuExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // menuLoad
            // 
            this.menuLoad.Name = "menuLoad";
            this.menuLoad.Size = new System.Drawing.Size(100, 22);
            this.menuLoad.Text = "&Load";
            this.menuLoad.Click += new System.EventHandler(this.menuLoad_Click);
            // 
            // menuSave
            // 
            this.menuSave.Name = "menuSave";
            this.menuSave.Size = new System.Drawing.Size(100, 22);
            this.menuSave.Text = "&Save";
            this.menuSave.Click += new System.EventHandler(this.menuSave_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(100, 22);
            this.menuExit.Text = "E&xit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuDisplay});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.listToolStripMenuItem.Text = "&Stats";
            // 
            // menuDisplay
            // 
            this.menuDisplay.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menuDisplay.Name = "menuDisplay";
            this.menuDisplay.Size = new System.Drawing.Size(112, 22);
            this.menuDisplay.Text = "&Display";
            this.menuDisplay.Click += new System.EventHandler(this.menuDisplay_Click);
            // 
            // gbNewStudent
            // 
            this.gbNewStudent.Controls.Add(this.rbFemale);
            this.gbNewStudent.Controls.Add(this.rbMale);
            this.gbNewStudent.Controls.Add(this.lblGender);
            this.gbNewStudent.Controls.Add(this.lblStudent);
            this.gbNewStudent.Controls.Add(this.lblAge);
            this.gbNewStudent.Controls.Add(this.lblName);
            this.gbNewStudent.Controls.Add(this.txtName);
            this.gbNewStudent.Controls.Add(this.numAge);
            this.gbNewStudent.Controls.Add(this.cmboStudent);
            this.gbNewStudent.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbNewStudent.Location = new System.Drawing.Point(9, 26);
            this.gbNewStudent.Name = "gbNewStudent";
            this.gbNewStudent.Size = new System.Drawing.Size(265, 135);
            this.gbNewStudent.TabIndex = 4;
            this.gbNewStudent.TabStop = false;
            this.gbNewStudent.Text = "User Input";
            // 
            // rbFemale
            // 
            this.rbFemale.AutoSize = true;
            this.rbFemale.Location = new System.Drawing.Point(165, 109);
            this.rbFemale.Name = "rbFemale";
            this.rbFemale.Size = new System.Drawing.Size(67, 21);
            this.rbFemale.TabIndex = 5;
            this.rbFemale.TabStop = true;
            this.rbFemale.Text = "Female";
            this.rbFemale.UseVisualStyleBackColor = true;
            // 
            // rbMale
            // 
            this.rbMale.AutoSize = true;
            this.rbMale.Location = new System.Drawing.Point(84, 110);
            this.rbMale.Name = "rbMale";
            this.rbMale.Size = new System.Drawing.Size(55, 21);
            this.rbMale.TabIndex = 4;
            this.rbMale.TabStop = true;
            this.rbMale.Text = "Male";
            this.rbMale.UseVisualStyleBackColor = true;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(24, 111);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(57, 17);
            this.lblGender.TabIndex = 3;
            this.lblGender.Text = "Gender: ";
            // 
            // lblStudent
            // 
            this.lblStudent.AutoSize = true;
            this.lblStudent.Location = new System.Drawing.Point(21, 84);
            this.lblStudent.Name = "lblStudent";
            this.lblStudent.Size = new System.Drawing.Size(59, 17);
            this.lblStudent.TabIndex = 3;
            this.lblStudent.Text = "Student: ";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(45, 57);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(37, 17);
            this.lblAge.TabIndex = 3;
            this.lblAge.Text = "Age: ";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(33, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Name: ";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(84, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(168, 24);
            this.txtName.TabIndex = 1;
            // 
            // numAge
            // 
            this.numAge.Location = new System.Drawing.Point(84, 55);
            this.numAge.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.numAge.Name = "numAge";
            this.numAge.Size = new System.Drawing.Size(65, 24);
            this.numAge.TabIndex = 2;
            // 
            // cmboStudent
            // 
            this.cmboStudent.FormattingEnabled = true;
            this.cmboStudent.Items.AddRange(new object[] {
            "Yes - Full time",
            "Yes - Part time",
            "Withdrawn"});
            this.cmboStudent.Location = new System.Drawing.Point(84, 83);
            this.cmboStudent.Name = "cmboStudent";
            this.cmboStudent.Size = new System.Drawing.Size(168, 23);
            this.cmboStudent.TabIndex = 3;
            // 
            // rbEnglish
            // 
            this.rbEnglish.AutoSize = true;
            this.rbEnglish.Location = new System.Drawing.Point(62, 70);
            this.rbEnglish.Margin = new System.Windows.Forms.Padding(2);
            this.rbEnglish.Name = "rbEnglish";
            this.rbEnglish.Size = new System.Drawing.Size(89, 21);
            this.rbEnglish.TabIndex = 7;
            this.rbEnglish.TabStop = true;
            this.rbEnglish.Text = "English 101";
            this.rbEnglish.UseVisualStyleBackColor = true;
            // 
            // rbMath
            // 
            this.rbMath.AutoSize = true;
            this.rbMath.Location = new System.Drawing.Point(152, 68);
            this.rbMath.Margin = new System.Windows.Forms.Padding(2);
            this.rbMath.Name = "rbMath";
            this.rbMath.Size = new System.Drawing.Size(81, 21);
            this.rbMath.TabIndex = 7;
            this.rbMath.TabStop = true;
            this.rbMath.Text = "Math 305";
            this.rbMath.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 71);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Class: ";
            // 
            // gbMath
            // 
            this.gbMath.Controls.Add(this.listMath);
            this.gbMath.Location = new System.Drawing.Point(298, 168);
            this.gbMath.Name = "gbMath";
            this.gbMath.Size = new System.Drawing.Size(250, 116);
            this.gbMath.TabIndex = 7;
            this.gbMath.TabStop = false;
            this.gbMath.Text = "Math 305";
            // 
            // listMath
            // 
            this.listMath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listMath.Location = new System.Drawing.Point(3, 16);
            this.listMath.Margin = new System.Windows.Forms.Padding(2);
            this.listMath.MultiSelect = false;
            this.listMath.Name = "listMath";
            this.listMath.Size = new System.Drawing.Size(244, 97);
            this.listMath.TabIndex = 0;
            this.listMath.UseCompatibleStateImageBehavior = false;
            this.listMath.View = System.Windows.Forms.View.List;
            this.listMath.SelectedIndexChanged += new System.EventHandler(this.listMath_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAdd.Location = new System.Drawing.Point(192, 104);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnReset
            // 
            this.btnReset.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnReset.Location = new System.Drawing.Point(5, 104);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 9;
            this.btnReset.Text = "&Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // gbEnglish
            // 
            this.gbEnglish.Controls.Add(this.listEnglish);
            this.gbEnglish.Location = new System.Drawing.Point(9, 168);
            this.gbEnglish.Name = "gbEnglish";
            this.gbEnglish.Size = new System.Drawing.Size(250, 116);
            this.gbEnglish.TabIndex = 7;
            this.gbEnglish.TabStop = false;
            this.gbEnglish.Text = "English 101";
            // 
            // listEnglish
            // 
            this.listEnglish.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listEnglish.Location = new System.Drawing.Point(3, 16);
            this.listEnglish.Margin = new System.Windows.Forms.Padding(2);
            this.listEnglish.MultiSelect = false;
            this.listEnglish.Name = "listEnglish";
            this.listEnglish.Size = new System.Drawing.Size(244, 97);
            this.listEnglish.TabIndex = 0;
            this.listEnglish.UseCompatibleStateImageBehavior = false;
            this.listEnglish.View = System.Windows.Forms.View.List;
            this.listEnglish.SelectedIndexChanged += new System.EventHandler(this.listEnglish_SelectedIndexChanged);
            // 
            // gbClassSelection
            // 
            this.gbClassSelection.Controls.Add(this.label2);
            this.gbClassSelection.Controls.Add(this.rbEnglish);
            this.gbClassSelection.Controls.Add(this.rbMath);
            this.gbClassSelection.Controls.Add(this.label1);
            this.gbClassSelection.Controls.Add(this.btnReset);
            this.gbClassSelection.Controls.Add(this.btnAdd);
            this.gbClassSelection.Font = new System.Drawing.Font("Calibri", 10F);
            this.gbClassSelection.Location = new System.Drawing.Point(279, 26);
            this.gbClassSelection.Margin = new System.Windows.Forms.Padding(2);
            this.gbClassSelection.Name = "gbClassSelection";
            this.gbClassSelection.Padding = new System.Windows.Forms.Padding(2);
            this.gbClassSelection.Size = new System.Drawing.Size(271, 135);
            this.gbClassSelection.TabIndex = 10;
            this.gbClassSelection.TabStop = false;
            this.gbClassSelection.Text = "Selection";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11F);
            this.label2.Location = new System.Drawing.Point(31, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(213, 36);
            this.label2.TabIndex = 10;
            this.label2.Text = "Choose whether this student will\r\nbe in Math 305 or English 101.";
            // 
            // btnToEnglish
            // 
            this.btnToEnglish.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToEnglish.Location = new System.Drawing.Point(263, 183);
            this.btnToEnglish.Margin = new System.Windows.Forms.Padding(2);
            this.btnToEnglish.Name = "btnToEnglish";
            this.btnToEnglish.Size = new System.Drawing.Size(30, 26);
            this.btnToEnglish.TabIndex = 11;
            this.btnToEnglish.Text = "<";
            this.btnToEnglish.UseVisualStyleBackColor = true;
            this.btnToEnglish.Click += new System.EventHandler(this.btnToEnglish_Click);
            // 
            // btnToMath
            // 
            this.btnToMath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToMath.Location = new System.Drawing.Point(263, 242);
            this.btnToMath.Margin = new System.Windows.Forms.Padding(2);
            this.btnToMath.Name = "btnToMath";
            this.btnToMath.Size = new System.Drawing.Size(30, 26);
            this.btnToMath.TabIndex = 11;
            this.btnToMath.Text = ">";
            this.btnToMath.UseVisualStyleBackColor = true;
            this.btnToMath.Click += new System.EventHandler(this.btnToMath_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(263, 213);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(30, 26);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "X";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnAdd;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 294);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnToMath);
            this.Controls.Add(this.btnToEnglish);
            this.Controls.Add(this.gbClassSelection);
            this.Controls.Add(this.gbEnglish);
            this.Controls.Add(this.gbMath);
            this.Controls.Add(this.gbNewStudent);
            this.Controls.Add(this.menuStripCustomEvents);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Student";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStripCustomEvents.ResumeLayout(false);
            this.menuStripCustomEvents.PerformLayout();
            this.gbNewStudent.ResumeLayout(false);
            this.gbNewStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).EndInit();
            this.gbMath.ResumeLayout(false);
            this.gbEnglish.ResumeLayout(false);
            this.gbClassSelection.ResumeLayout(false);
            this.gbClassSelection.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripCustomEvents;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuDisplay;
        private System.Windows.Forms.GroupBox gbNewStudent;
        public System.Windows.Forms.RadioButton rbFemale;
        public System.Windows.Forms.RadioButton rbMale;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblStudent;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblName;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.NumericUpDown numAge;
        public System.Windows.Forms.ComboBox cmboStudent;
        private System.Windows.Forms.GroupBox gbMath;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ListView listMath;
        private System.Windows.Forms.GroupBox gbEnglish;
        private System.Windows.Forms.ListView listEnglish;
        private System.Windows.Forms.RadioButton rbEnglish;
        private System.Windows.Forms.RadioButton rbMath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbClassSelection;
        private System.Windows.Forms.ToolStripMenuItem menuLoad;
        private System.Windows.Forms.ToolStripMenuItem menuSave;
        private System.Windows.Forms.Button btnToEnglish;
        private System.Windows.Forms.Button btnToMath;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label2;
    }
}

