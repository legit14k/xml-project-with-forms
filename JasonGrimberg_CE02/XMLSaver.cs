﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace JasonGrimberg_CE02
{
    // Can't seem to get the data to save out the way I'd like.
    // Will try again some other time...Mr. Lewis, Any tips or notes on what
    // I was doing wrong or something that I could add would be a great
    // help.
    public static class XMLSaver
    {
        public static void SerializeToXML(List<myObject> ListviewItems, string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<myObject>));
            TextWriter textWriter = new StreamWriter(path);
            serializer.Serialize(textWriter, ListviewItems);
            textWriter.Close();
        }

        public static List<myObject> DeserializeFromXML(string path)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(List<myObject>));
            TextReader textReader = new StreamReader(path);
            List<myObject> ListviewItems;
            ListviewItems = (List<myObject>)deserializer.Deserialize(textReader);
            textReader.Close();

            return ListviewItems;
        }
    }
}
