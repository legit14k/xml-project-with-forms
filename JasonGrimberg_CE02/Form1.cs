﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
/// <summary>
/// Jason Grimberg
/// CE02 Passing data and multiple forms
/// </summary>
namespace JasonGrimberg_CE02
{
    public partial class MainForm : Form
    {
        // Create list to hold all variables from the load file
        public List<myObject> listViewItems = new List<myObject>();

        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        public EventHandler ObjectAdded;
        public EventHandler ObjectToEnglish;
        public EventHandler ObjectToMath;
        public EventHandler SelectionChanged;

        // -----------------------------------------------------------------------------
        // Student data components
        // -----------------------------------------------------------------------------
        // New student data
        public Students Data
        {
            // Get all of the student data
            get
            {
                Students s = new Students();
                s.StudentName = txtName.Text;
                s.StudentAge = numAge.Value;
                s.StudentStatus = cmboStudent.Text;
                s.StatusIndex = cmboStudent.SelectedIndex;
                s.StudentMale = rbMale.Checked;
                s.StudentFemale = rbFemale.Checked;
                s.StudentEnglish = rbEnglish.Checked;
                s.StudentMath = rbMath.Checked;
                return s;
            }

            // Set all of the student data
            set
            {
                txtName.Text = value.StudentName;
                numAge.Value = value.StudentAge;
                cmboStudent.Text = value.StudentStatus;
                rbMale.Checked = value.StudentMale;
                rbFemale.Checked = value.StudentFemale;
                rbEnglish.Checked = value.StudentEnglish;
                rbMath.Checked = value.StudentMath;
            }
        }

        // Selected object check
        public Students SelectedObject
        {
            // Get the selected object
            get
            {
                // Fill in the data fields with the selected item
                if (listMath.SelectedItems.Count > 0)
                {
                    // Return the math list if selected
                    return listMath.SelectedItems[0].Tag as Students;
                }
                else if (listEnglish.SelectedItems.Count > 0)
                {
                    // Return the english list if selected
                    return listEnglish.SelectedItems[0].Tag as Students;
                }
                else
                {
                    // Reset all of the outputs
                    ResetInputs();
                    // Return nothing if nothing is selected
                    return new Students();
                }

            }
        }
        // -----------------------------------------------------------------------------
        // Initialize MainForm
        // -----------------------------------------------------------------------------
        public MainForm()
        {
            InitializeComponent();

            // Clear all inputs just to be safe
            ResetInputs();
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------

        // Main form load trigger
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Sub to the math list
            ObjectToMath += ObjectToMathHandler;

            // Sub to the english list
            ObjectToEnglish += ObjectToEnglishHandler;

            // Sub to the object added handler
            ObjectAdded += ObjectAddedHandler;

            // Sub to the selection handler
            SelectionChanged += SelectionChangedHandler;
        }

        // When an item is selected in the english list
        private void listEnglish_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Enable the buttons that the user can use
            btnToMath.Enabled = true;
            btnDelete.Enabled = true;

            // Invoke the selection
            SelectionChanged?.Invoke(this, new EventArgs());
        }

        // When an item is selected in the math list
        private void listMath_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Enable the buttons that the user can use
            btnToEnglish.Enabled = true;
            btnDelete.Enabled = true;

            // Invoke the selection
            SelectionChanged?.Invoke(this, new EventArgs());
        }

        // Display a window of how many items are in both listss
        private void menuDisplay_Click(object sender, EventArgs e)
        {
            // Message box to show the totals
            MessageBox.Show("Math Students: " + listMath.Items.Count.ToString()
                + "\r\nEnglish Students: " + listEnglish.Items.Count.ToString(), 
                "Totals", MessageBoxButtons.OK);
        }

        // Reset all of the user input controls
        private void btnReset_Click(object sender, EventArgs e)
        {
            // Call the reset method
            ResetInputs();
        }

        // Add to one of the lists
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Add to list method call
            AddToList();
            // Reset all inputs after add call is completed
            ResetInputs();
        }

        // Button to add to the english list
        private void btnToEnglish_Click(object sender, EventArgs e)
        {
            // Add the object to the english list
            if (listMath.SelectedItems.Count > 0)
            {
                // Null check for the add method
                if (ObjectToEnglish != null)
                {
                    ObjectToEnglish(this, new EventArgs());
                }
            }
            
        }

        // Button to add to the math list
        private void btnToMath_Click(object sender, EventArgs e)
        {
            // Add the object to the math list
            if (listEnglish.SelectedItems.Count > 0)
            {
                // Null check for the add method
                if (ObjectToMath != null)
                {
                    ObjectToMath(this, new EventArgs());
                }
            }
        }

        // Delete button for the current selected object
        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Remove the selected item in math if selected
            if (listMath.SelectedItems.Count > 0)
            {
                // Remove at current selection
                foreach(ListViewItem i in listMath.SelectedItems)
                {
                    listMath.Items.Remove(i);
                }
                
            }
            // Remove the selected item in english if selected
            else if (listEnglish.SelectedItems.Count > 0)
            {
                // Remove at current selection
                foreach(ListViewItem i in listEnglish.SelectedItems)
                {
                    listEnglish.Items.Remove(i);
                }
            }
            else
            {
                // Message box to show that nothing is selected
                MessageBox.Show("Please select an item to delete.");
                return;
            }
        }

        // Load event
        private void menuLoad_Click(object sender, EventArgs e)
        {
            // Call load file method
            LoadFile();
        }

        // Save event
        private void menuSave_Click(object sender, EventArgs e)
        {
            // Call save file method
            SaveFile();
        }

        // Exit event
        private void menuExit_Click(object sender, EventArgs e)
        {
            // Close out the application
            Application.Exit();
        }
        
        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------

        // Object added event handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Main form as the sender
            MainForm main = sender as MainForm;
            // Set students as the main forms data
            Students s = main.Data;
            // Set new list view items
            ListViewItem lvi = new ListViewItem();

            // Set the list view items as strings
            lvi.Text = s.ToString();
            // Change the tag as the new string
            lvi.Tag = s;

            // Choose whether the object will be added to the 
            // math or english list
            if(rbMath.Checked == true)
            {
                // Add to math list
                listMath.Items.Add(lvi);
            }
            else if(rbEnglish.Checked == true)
            {
                // Add to english list
                listEnglish.Items.Add(lvi);
            }
            else
            {
                // Message box to show that the user had not selected 
                // the correct input control
                MessageBox.Show("Please choose where this student will go.");
                return;
            }
        }

        // To english event handler
        public void ObjectToEnglishHandler(object sender, EventArgs e)
        {
            // Set the radio button to true
            rbEnglish.Checked = true;
            // Set the main form as sender
            MainForm main = sender as MainForm;
            // Set the student data to the main form data
            Students s = main.Data;
            // Create new list view items
            ListViewItem lvi = new ListViewItem();
            
            // Set the new list view to a string
            lvi.Text = s.ToString();
            // Set the tag as the new string
            lvi.Tag = s;

            // Add to the english list
            listEnglish.Items.Add(lvi);

            // Remove after the transfer is complete
            foreach (ListViewItem i in listMath.SelectedItems)
            {
                listMath.Items.Remove(i);
            }
        }

        // To math event handler
        public void ObjectToMathHandler(object sender, EventArgs e)
        {
            // Set the radio button to true
            rbMath.Checked = true;
            // Set the main form as sender
            MainForm main = sender as MainForm;
            // Set the student data to the main form data
            Students s = main.Data;
            // Create new list view items
            ListViewItem lvi = new ListViewItem();

            // Set the new list view to a string
            lvi.Text = s.ToString();
            // Set the tag as the new string
            lvi.Tag = s;

            // Add to the math list
            listMath.Items.Add(lvi);

            // Remove after the transfer is complete
            foreach (ListViewItem i in listEnglish.SelectedItems)
            {
                listEnglish.Items.Remove(i);
            }
        }

        // Call the selected change handler
        private void SelectionChangedHandler(object sender, EventArgs e)
        {
            // Set the data as the selected object
            Data = SelectedObject;
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Reset all of the user controls method
        public void ResetInputs()
        {
            btnDelete.Enabled = false;
            btnToEnglish.Enabled = false;
            btnToMath.Enabled = false;
            Data = new Students();
        }

        // Add to a list method
        public void AddToList()
        {
            // Error check to make sure the user had filled out all user input controls
            if (txtName.Text != "" && numAge.Value != 0 && cmboStudent.SelectedIndex != -1)
            {
                // Null check for object added
                if (ObjectAdded != null)
                {
                    ObjectAdded(this, new EventArgs());
                }
            }
        }

        // Save file method
        public void SaveFile()
        {
            // Use the SaveFileDialog to open a save dialog box
            using (var sfd = new SaveFileDialog())
            {
                // Variable to save file name
                string filename = "";

                // Show only text files to write out to
                sfd.Filter = "Text files (*.txt)|*.txt";

                // Change the title of the save dialog 
                sfd.Title = "Save a Text File";

                // Check for the save file dialog to be okay
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    // Set the filename to what is shown in dialog
                    filename = sfd.FileName.ToString();

                    // Another check to make sure it is not null
                    if (filename != "")
                    {
                        // Write to the file both lists of data
                        using (StreamWriter sw = new StreamWriter(filename))
                        {
                            foreach (ListViewItem item in listMath.Items)
                            {
                                sw.WriteLine(item.SubItems[0].Text);
                            }
                            foreach (ListViewItem item in listEnglish.Items)
                            {
                                sw.WriteLine(item.SubItems[0].Text);
                            }
                        }
                    }
                }
            }
        }

        // Load file method
        public void LoadFile()
        {
            // Open dialog new instance
            OpenFileDialog saveDialog = new OpenFileDialog();

            // Change the title of the open dialog
            saveDialog.Title = "Open Student Text File";

            // Filter out just text files to open
            saveDialog.Filter = "TXT files|*.txt";

            // Show the initial directory when the open dialog opens up
            saveDialog.InitialDirectory = @"C:\";

            // Doesn't seem to work as planned
            //// If statement for the opening of the open dialog
            //if (saveDialog.ShowDialog() == DialogResult.OK)
            //{
            //    string filename = saveDialog.FileName;

            //    // Create an array of all the lines in the text box
            //    string[] filelines = File.ReadAllLines(filename);

            //    foreach (string line in filelines)
            //    {
            //        listEnglish.Items.Add(line);
            //    }
            //}
        }
    }

    // Create an object class to set new columns in lists
    // Can't seem to get it to work, try again later
    public class myObject
    {
        public string Col1 { get; set; }
        public string Col2 { get; set; }
        public string Col3 { get; set; }
    }
}
