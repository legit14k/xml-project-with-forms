﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JasonGrimberg_CE02
{
    public class Students
    {
        // All Student Variables
        string studentName;
        decimal studentAge;
        string studentStatus;
        bool studentMale;
        bool studentFemale;
        bool studentMath;
        bool studentEnglish;
        int statusIndex;

        // All student constructors
        public string StudentName { get => studentName; set => studentName = value; }
        public decimal StudentAge { get => studentAge; set => studentAge = value; }
        public string StudentStatus { get => studentStatus; set => studentStatus = value; }
        public bool StudentMale { get => studentMale; set => studentMale = value; }
        public bool StudentFemale { get => studentFemale; set => studentFemale = value; }
        public int StatusIndex { get => statusIndex; set => statusIndex = value; }
        public bool StudentMath { get => studentMath; set => studentMath = value; }
        public bool StudentEnglish { get => studentEnglish; set => studentEnglish = value; }

        // Tostring override function for the output
        public override string ToString()
        {
            string newStudentName = StudentName.ToString();
            string newStudentAge = StudentAge.ToString();
            string newStudentStatus = StudentStatus.ToString();
            
            return newStudentName + " " + newStudentAge + " " + newStudentStatus;
        }
    }
}
